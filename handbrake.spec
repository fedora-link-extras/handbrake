Name:		handbrake
Version:	0.10.5
Release:	1%{?dist}
Summary:	The open source video transcoder
Group:		Applications/Multimedia

License:	GPLv2
URL:		http://handbrake.fr
Source0:	https://handbrake.fr/mirror/HandBrake-%{version}.tar.bz2

BuildRequires:	cmake
BuildRequires:	yasm
BuildRequires:	intltool
BuildRequires:	libtool
BuildRequires:	gtk3-devel
BuildRequires:	libnotify-devel
BuildRequires:	dbus-glib-devel
BuildRequires:	libgudev-devel
BuildRequires:	libxml2-devel
BuildRequires:	libtheora-devel
BuildRequires:	libvorbis-devel
BuildRequires:	lame-devel
BuildRequires:	x264-devel
BuildRequires:	libass-devel
BuildRequires:	libsamplerate-devel
BuildRequires:	bzip2-devel
BuildRequires:	desktop-file-utils

%description
HandBrake is a tool for converting video from nearly any format to a selection
of modern, widely supported codecs.


%package cli
Summary:	Commandline interface to the open source video transcoder
Group:		Applications/Multimedia


%description cli
HandBrake is a tool for converting video from nearly any format to a selection
of modern, widely supported codecs.

This is a command-line interface to HandBrake.


%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%prep
%autosetup -n HandBrake-%{version}


%build
./configure --prefix=%_prefix
cd build
%make_build


%install
rm -rf %{buildroot}
cd build
%make_install

%files
%doc README.pod NEWS THANKS CREDITS AUTHORS
%license COPYING
%{_bindir}/ghb
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/scalable/apps/*
%lang(cs) %{_datadir}/locale/cs/LC_MESSAGES/ghb.mo
%lang(da) %{_datadir}/locale/da/LC_MESSAGES/ghb.mo
%lang(de) %{_datadir}/locale/de/LC_MESSAGES/ghb.mo
%lang(es) %{_datadir}/locale/es/LC_MESSAGES/ghb.mo
%lang(fr) %{_datadir}/locale/fr/LC_MESSAGES/ghb.mo
%lang(it_IT) %{_datadir}/locale/it_IT/LC_MESSAGES/ghb.mo
%lang(ja_JP) %{_datadir}/locale/ja_JP/LC_MESSAGES/ghb.mo
%lang(ko) %{_datadir}/locale/ko/LC_MESSAGES/ghb.mo
%lang(no) %{_datadir}/locale/no/LC_MESSAGES/ghb.mo
%lang(pt_BR) %{_datadir}/locale/pt_BR/LC_MESSAGES/ghb.mo
%lang(ro_RO) %{_datadir}/locale/ro_RO/LC_MESSAGES/ghb.mo
%lang(ru) %{_datadir}/locale/ru/LC_MESSAGES/ghb.mo
%lang(th) %{_datadir}/locale/th/LC_MESSAGES/ghb.mo
%lang(zh_CN) %{_datadir}/locale/zh_CN/LC_MESSAGES/ghb.mo


%files cli
%doc
%{_bindir}/HandBrakeCLI

%changelog
* Wed Feb 17 2016 Link Dupont <link@fastmail.com> - 0.10.5-1
- New upstream version
- Add missing BuildRequires
* Tue Sep 22 2015 Link Dupont <link.dupont@me.com> - 0.10.2-1
- Initial package

